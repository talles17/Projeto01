/**
 * @file	abre_arq_matriz1024x1024.cpp
 * @brief	Arquivo que obtem os dados das matrizes A e B 1024x1024, multiplica as duas e grava o resultado da 
 *			multiplicacao no arquivo C1024x1024.txt, e obtem os tempos de execuçao do algoritimo da multiplicaçao
 *			interativa das matrizes 		
 * @author	Cleydson Talles Araujo Vieira
 * @since	27/04/2017
 * @date 	01/05/2017
 * @sa 		abre_arquivos.h	
 */

#include <iostream>
using std::cout ;
using std::endl ;
using std::cerr ;

#include <iomanip>
using std::fixed ;
using std::setprecision ;
using std::scientific ;

#include <chrono>
using namespace std::chrono ;

#include <ratio>
#include <ctime>
#include <cmath>

	
#include <fstream>
using std::ifstream ;
using std::ofstream ;

#include <cstdlib>

#include "matrizes.h"
#include "abre_arquivos.h"
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C1024x1024, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/


void matriz1024x1024 (double **v) {
	int la ,ca , lb , cb ;
	ifstream matrizA ("/home/talles/bti/lp/projeto01/input/A1024x1024.txt") ;
	if (matrizA.bad() || !matrizA || (matrizA.is_open() == 0)) {
		cerr << "O arquivo da matriz1024x1024 nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;

	}

	matrizA >> la >> ca ;
	int** a = new int*[la] ;
	for (int i = 0 ; i < la ; i++) {
		a[i] = new int[ca] ;
	}

	for (int i = 0 ; i < la ; i++) {
		for (int j = 0 ; j < ca ; j++) {
			matrizA >> a[i][j] ;
		}
	}

	matrizA.close() ;

	ifstream matrizB ("/home/talles/bti/lp/projeto01/input/B1024x1024.txt") ;
	if (matrizB.bad() || !matrizB || (matrizB.is_open() == 0)) {
		cerr << "O arquivo b nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;
	}

	matrizB >> lb >> cb ;
	int** b = new int*[lb] ;
	for (int i =0 ; i < lb ; i++) {
		b[i] = new int[cb] ;
	}

	for (int i = 0 ; i < lb ; i++) {
		for (int j = 0 ; j < cb ; j++) {
			matrizB >> b[i][j] ;
		}
	}

	matrizB.close() ;

	

	int** c = 0 ; 


	for (int i = 1 ; i <= 20 ; i++) {
		high_resolution_clock::time_point inicio = high_resolution_clock::now() ;
		c = multiplicaI  (a , b , 1024) ;
		high_resolution_clock::time_point fim = high_resolution_clock::now() ;
		duration<double> tempo = duration_cast<duration<double>>(fim - inicio) ;
		v[9][i-1] = tempo.count() ;
		
	}


	for (int i = 0 ; i < la ; i++) {
		delete [] a[i] ;
	}
	delete [] a;

	for (int i = 0 ; i < lb ; i++) {
		delete [] b[i] ;
	}
	delete [] b;

	ofstream saida("/home/talles/bti/lp/projeto01/data/matrizesC/C1024x1024.txt") ;
	if (saida.bad() || !saida || (saida.is_open() == 0)) {
		cerr << "O arquivo c nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit(1) ;
	}

	saida << la << " " << cb << endl ;
	for (int ii = 0 ; ii < la ; ii++) {
		for (int jj = 0 ; jj < ca ; jj++) {
			saida << c[ii][jj] << " " ;
		}
		saida << endl ;	
	}

	saida.close() ;

	for (int i = 0 ; i < la ; i++) {
		delete [] c[i] ;
	}
	delete [] c;

	
}