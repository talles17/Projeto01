/**
 * @file	main.cpp
 * @brief	Codigo fonte principal do programa		
 * @author	Cleydson Talles Araujo Vieira
 * @since	27/04/2017
 * @date 	01/05/2017
 */

#include <iostream>
using std::cout ;
using std::endl ;
using std::cerr ;

#include <cstdlib> 

#include <fstream>
using std::ifstream ;

#include "abre_arquivos.h"

#include "matrizes.h"

/** @brief Funçao principal do programa */
int main (int argc, char* argv[]) {

	if(argc != 11) {
		cerr << "Execucao incorreta do programa" << endl ;
		exit(1) ;
	}

	else {
		
		double** tempo = new double*[20] ;
		for (int i = 0 ; i < 20 ; i++) {
			tempo[i] = new double[10] ;
		}

		matriz2x2(tempo) ;
		matriz4x4(tempo) ;
		matriz8x8(tempo) ;
		matriz16x16 (tempo) ;	
   	 	matriz32x32 (tempo) ;	
   	 	matriz64x64 (tempo) ;	
   	 	matriz128x128 (tempo) ;	
   	 	matriz256x256 (tempo) ;	
   	 	matriz512x512 (tempo) ;	
   	 	matriz1024x1024 (tempo) ; 

		estatistica(tempo) ;

		/*for (int i = 0 ; i < 20 ; i++) {
			delete [] tempo[i] ;
		}
		delete [] tempo ; */ 
	} 

	
	return 0 ;
}