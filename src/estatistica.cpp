/**
 * @file	abre_arq_matriz1024x1024.cpp
 * @brief	Arquivo que gera os dados dos resultados de cada execucao feita pelas matrizes 		
 * @author	Cleydson Talles Araujo Vieira
 * @since	01/05/2017
 * @date 	01/05/2017
 * @sa 		abre_arquivos.h	
 */

#include <iostream>
using std::cout ;
using std::endl ;
using std::cerr ;

#include <fstream>
using std::ifstream ;
using std::ofstream ;

#include <cstdlib>

#include <iomanip>
using std::fixed ;
using std::setprecision ;
using std::scientific ;

#include <cmath>
/**
 * @details Com os valores do tempo de execuçao armazenados na matriz, o procedimento ira registrar todos os dados
 *			de cada matriz no arquivo stats-ite.dat	
 * @param   V Matriz com os valores de execuçao	
*/
void estatistica (double **v ) {
	
	ofstream est("/home/talles/bti/lp/projeto01/data/stats-ite.dat") ;
	if (est.bad() || !est || (est.is_open() == 0)) {
		cerr << "O arquivo c nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit(1) ;
	}
	for (int x = 0 ; x < 10 ; x++) {
		int indice = 2 ;
		double media = 0 , tempo_medio , aux = 0  , desvio_padrao ;
		for (int i = 0 ; i < 20 ; i++) {
			media += v[x][i] ;
		}
		tempo_medio = media/20 ;
		for (int i = 0 ; i < 20 ; i++) {
			aux += pow((v[x][i] - tempo_medio), 2) ;
		}
		aux = aux/20 ;
		desvio_padrao = sqrt(aux) ;

		indice = pow(indice,x+1) ;

		est << "matriz"<<indice<<"x"<< indice << " - " ;
		for (int i = 1 ; i <= 20 ; i++) {
			est << i << " tempo: " << fixed << setprecision(9) << v[x][i-1] << " " ; 
		}
		est << "tempo medio: " << fixed << setprecision(9) << tempo_medio << " " ;
		est << scientific << "desvio padrao: " << desvio_padrao << endl ;
		est << endl << endl ;
	}
	est.close() ;


}