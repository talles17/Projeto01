/**
 * @file	matrizes.h
 * @brief	Implementaçao da funçao que multiplica matrizes no modo interativo		
 * @author	Cleydson Talles Araujo Vieira
 * @since	27/04/2017
 * @date 	01/05/2017
 */
#ifndef MATRIZES_H
#define MATRIZES_H

#include <iostream>
using std::cerr ;
using std::endl ;

#include <cstdlib>

/**
 * @brief Funão generica que realiza a multiplicaçao de matrizes
 * @param A Matriz a
 * @param B Matriz b
 * @param n Dimensao da matriz
*/
template<typename T>
T** multiplicaI (T** A , T** B , int n ) {
	int i , j , k , **c;
	c = new int*[n] ;
	if (!c) {
		cerr << "erro na alocacao de memoria da matriz c" << endl ;
		exit (1) ;
	}
	for (i = 0 ; i < n ; i++) {
		c[i] = new int[n] ;
	}


	for (i = 0 ; i < n  ; i++) {
		for (j = 0 ; j < n ; j++) {
			int soma = 0 ;
			for (k = 0 ; k < n ; k++) {
				soma += A[i][k] * B[k][j] ;
			}
			c[i][j] = soma ;
		}
	}

	return c ;
}

/*template <typename T>
T** multiplicaR (T** A, T** B , T** C , int n) {
	int i , j , k ;
	
	if (n == 1) {
		C[0][0] = A[0][0] * B[0][0] ;
	}
	else {

		T A11[n/2][n/2] , A12[n/2][n/2] , A21[n/2][n/2] , A22[n/2][n/2] ;
		T B11[n/2][n/2] , B12[n/2][n/2] , B21[n/2][n/2] , B22[n/2][n/2] ;
		T C11[n/2][n/2] , C12[n/2][n/2] , C21[n/2][n/2] , C22[n/2][n/2] ;
		T P1[n/2][n/2] , P2[n/2][n/2] , P3[n/2][n/2] , P4[n/2][n/2] ,
		  P5[n/2][n/2] , P6[n/2][n/2] , P7[n/2][n/2] , P8[n/2][n/2] ;

		multiplicaR (A11 , B11, P1, n/2) ;
		multiplicaR (A12 , B21, P2, n/2) ;
		multiplicaR (A11 , B12, P3, n/2) ;
		multiplicaR (A12 , B22, P4, n/2) ;
		multiplicaR (A21 , B11, P5, n/2) ;
		multiplicaR (A22 , B21, P6, n/2) ;
		multiplicaR (A21 , B12, P7, n/2) ;
		multiplicaR (A22 , B22, P8, n/2) ;

		C11 = P1 + P2 ;
		C12 = P3 + P4 ;
 		C21 = P5 + P6 ;
 		C22 = P7 + P8 ;

 	}


} */



#endif /* MATRIZES_H */