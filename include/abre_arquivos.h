/**
 * @file	abre_arquivos.h
 * @brief	Definiçao das funcoes que operam nas matrizes		
 * @author	Cleydson Talles Araujo Vieira
 * @since	27/04/2017
 * @date 	01/05/2017
 */
#ifndef ABRE_ARQUIVOS_H
#define ABRE_ARQUIVOS_H

/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C2x2, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz2x2 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C4x4, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz4x4 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C8x8, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz8x8 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C16x16, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz16x16 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C32x32, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz32x32 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C64x64, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz64x64 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C128x128, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz128x128 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C256x256, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz256x256 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C512x512, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz512x512 (double **v) ;
/**
 * @details Os arquivos das matrizes a e b serao abertos, apartir desses arquivos serao 	
 *			retirados os valores das matrizes a e b , com esses valores sera feito o algoritimo
 *			de multiplicaçao entre as duas matrizes 20 vezes, e nesse looping sera registrado os 
 *			valores de tempo de execuçao a cada vez que o algoritimo for executado. no fim, o 
 *			resultado sera registrado no arquivo C1024x1024, e o espaço de memoria das matrizes serao
 *			apagadas.  	
 * @param	V matriz com os valores da medida do tempo de execução do algoritimo
*/
void matriz1024x1024 (double **v) ;

/**
 * @details Com os valores do tempo de execuçao armazenados na matriz, o procedimento ira registrar todos os dados
 *			de cada matriz no arquivo stats-ite.dat	
 * @param   V Matriz com os valores de execuçao	
*/
void estatistica (double **v ) ;


#endif /* ABRE_ARQUIVOS_H */