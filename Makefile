RM= rm -rf
CC=g++

LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test


CFLAGS= -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

.PHONY: all clean debug doxy doc

all: multimat


debug: CFLAGS += -g -O0
debug: multimat

multimat:$(OBJ_DIR)/abre_arq_matriz2x2.o $(OBJ_DIR)/abre_arq_matriz4x4.o $(OBJ_DIR)/abre_arq_matriz8x8.o $(OBJ_DIR)/abre_arq_matriz16x16.o $(OBJ_DIR)/abre_arq_matriz32x32.o $(OBJ_DIR)/abre_arq_matriz64x64.o $(OBJ_DIR)/abre_arq_matriz128x128.o $(OBJ_DIR)/abre_arq_matriz256x256.o $(OBJ_DIR)/abre_arq_matriz512x512.o $(OBJ_DIR)/abre_arq_matriz1024x1024.o $(OBJ_DIR)/estatistica.o $(OBJ_DIR)/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'multimat' criado em $(BIN_DIR)] +++"
	@echo "============="



$(OBJ_DIR)/abre_arq_matriz2x2.o: $(SRC_DIR)/abre_arq_matriz2x2.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz4x4.o: $(SRC_DIR)/abre_arq_matriz4x4.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz8x8.o: $(SRC_DIR)/abre_arq_matriz8x8.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz16x16.o: $(SRC_DIR)/abre_arq_matriz16x16.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz32x32.o: $(SRC_DIR)/abre_arq_matriz32x32.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz64x64.o: $(SRC_DIR)/abre_arq_matriz64x64.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz128x128.o: $(SRC_DIR)/abre_arq_matriz128x128.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz256x256.o: $(SRC_DIR)/abre_arq_matriz256x256.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz512x512.o: $(SRC_DIR)/abre_arq_matriz512x512.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/abre_arq_matriz1024x1024.o: $(SRC_DIR)/abre_arq_matriz1024x1024.cpp $(INC_DIR)/abre_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/estatistica.o: $(SRC_DIR)/estatistica.cpp 
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/main.o:	$(SRC_DIR)/main.cpp $(INC_DIR)/matrizes.h
	$(CC) -c $(CFLAGS) -o $@ $<

doxy:
	doxygen -g

doc:
	$(RM) $(DOC_DIR)/*
	doxygen



clean: 
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
